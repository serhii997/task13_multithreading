package com.view;

import com.controller.Controller;
import com.controller.ControllerImp;

import java.util.*;

public class MyView {
    private static Scanner scanner = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private ResourceBundle bundle;
    private Locale locale;
    private Controller controller;

    public MyView() {
        controller = new ControllerImp();
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu",locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::internationalizeMenuUkraine);
        methodMenu.put("2", this::internationalizeMenuEnglish);
        methodMenu.put("3", (() -> controller.pingPong()));
        methodMenu.put("4", (() -> controller.allTaskFibonacci(10)));
        methodMenu.put("5", (() -> controller.sleep()));
        methodMenu.put("6", (() -> controller.synchronize()));
        methodMenu.put("7", (() -> controller.pipe()));
        methodMenu.put("8", (() -> controller.synhronizeWithLock()));
        methodMenu.put("9", (() -> controller.myBlockQueue()));
        methodMenu.put("10", (() -> controller.myLock()));
        methodMenu.put("Q", (() -> exit()));
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1",bundle.getString("1"));
        menu.put("2",bundle.getString("2"));
        menu.put("3",bundle.getString("3"));
        menu.put("4",bundle.getString("4"));
        menu.put("5",bundle.getString("5"));
        menu.put("6",bundle.getString("6"));
        menu.put("7",bundle.getString("7"));
        menu.put("8",bundle.getString("8"));
        menu.put("9",bundle.getString("9"));
        menu.put("10",bundle.getString("10"));
        menu.put("Q",bundle.getString("Q"));
    }

    private void internationalizeMenuUkraine(){
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void internationalizeMenuEnglish(){
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);
        setMenu();
        printMenu();
    }

    private void exit(){
        System.exit(0);
    }

    private void printMenu(){
        menu.forEach((key, value) -> System.out.println("`"+key+"` - "+"\t"+value));
    }

    public void run(){
        String input ="";
        printMenu();
        do {
            try {
                input = MyView.scanner.next().toLowerCase();
                methodMenu.get(input).print();
            } catch (NullPointerException e) {
                printMenu();
            } catch (Exception e) {
                //LOGGER.error(e.getMessage());
            }
        } while (!input.equalsIgnoreCase("Q"));
    }
}
