package com.view;

public interface Printable {
    void print();
}
