package com.controller;

import com.model.*;

import java.io.IOException;

public class ControllerImp implements Controller {

    @Override
    public void pingPong() {
        new PingPong().show();
    }

    @Override
    public void allTaskFibonacci(int number) {
        Fibonacci fibonacci = new Fibonacci(number);
        fibonacci.show();
        fibonacci.sumFibonacci();
    }

    @Override
    public void sleep() {
        new RandomAmount().run();
    }

    @Override
    public void synchronize() {
        new Synchronize().run();
    }

    @Override
    public void synhronizeWithLock() {
        new SynchonizeWithLock().run();
    }

    @Override
    public void pipe() {
        try {
            new PipeCommunication().show();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void myLock() {
        new MyLock().show();
    }

    @Override
    public void myBlockQueue() {
        new MyBlockingQueue().run();
    }
}
