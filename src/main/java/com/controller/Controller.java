package com.controller;

public interface Controller {
    void pingPong();
    void allTaskFibonacci(int number );
    void sleep();
    void synchronize();
    void synhronizeWithLock();
    void pipe();
    void myLock();
    void myBlockQueue();
}
