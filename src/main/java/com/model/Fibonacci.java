package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Fibonacci {
    private static final Logger LOGGER = LogManager.getLogger(Synchronize.class);
    private int number;

    public Fibonacci(int number) {
        this.number = number;
    }

    public void show(){
        List<Future<Integer>> futureList = new ArrayList<>();

        Thread thread = new Thread((() -> System.out.println(searchNumber(number) + Thread.currentThread().getName())));
        thread.start();
        ExecutorService service = Executors.newFixedThreadPool(3);
        Future<Integer> future = service.submit(new MyCallable(number));
        service.submit(new WithRunnable(number));
        futureList.add(future);
        try {
            LOGGER.info("Callable = " + future.get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Future<Integer> futureFibonacci = service.submit(new ModernFibonnaci());
        futureList.add(futureFibonacci);
        futureList.forEach(LOGGER::info);
        for (Future future1 : futureList) {
            try {
                System.out.println("callabel  = " +future1.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        service.shutdown();

    }

    private int searchNumber(int number){
        if(number == 0){
            return 0;
        } else if(number == 1){
            return 1;
        } else if(number == 2){
            return 1;
        }else{
            return (searchNumber(number - 1) + searchNumber(number - 2));
        }
    }

    public int sumFibonacci(){
        List<Integer> allFibonacciNumber = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            allFibonacciNumber.add(searchNumber(i));
        }
        LOGGER.info(allFibonacciNumber.toString());
        Integer integer = allFibonacciNumber.stream().reduce((i, i2) -> i + i2).get();
        return integer;
    }

    class WithRunnable implements Runnable{
        private int number;

        public WithRunnable(int number) {
            this.number = number;
        }
        @Override
        public void run() {

            try {
                TimeUnit.SECONDS.sleep(2);
                LOGGER.info("Runnable number = " + searchNumber(number) + Thread.currentThread().getName());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class MyCallable implements Callable<Integer> {
        private int number;

        public MyCallable(int number) {
            this.number = number;
        }

        @Override
        public Integer call() throws Exception {
            TimeUnit.SECONDS.sleep(2);
            LOGGER.info("MyCallable = " + Thread.currentThread().getName());
            return searchNumber(number);
        }
    }

    class ModernFibonnaci implements Callable<Integer>{

        @Override
        public Integer call() throws Exception {
            LOGGER.info(Thread.currentThread().getName());
            return sumFibonacci();
        }
    }
}
