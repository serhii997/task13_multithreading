package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MyLock {
    private static final Logger log = LogManager.getLogger(MyLock.class);

    private Lock lock = new ReentrantLock();
    private List<Integer> list = new ArrayList<>();

    public void show(){
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 100000; i++) {
                addNumber(i);
            }
        });
        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 100000; i++) {
                addNumber(i);
            }
        });
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        list.forEach(log::info);
    }

    private void addNumber(int count){
        lock.lock();
        list.add(count);
        lock.unlock();
    }

    public static void main(String[] args) {
        new MyLock().show();
    }
}
