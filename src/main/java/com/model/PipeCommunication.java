package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeCommunication {

    public void show() throws IOException, InterruptedException {
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream(pipedOutputStream);

        Producer producer = new Producer(pipedOutputStream);
        Consumer consumer = new Consumer(pipedInputStream);

        Thread pThread = new Thread(producer);
        Thread cThread = new Thread(consumer);

        pThread.start();
        cThread.start();

        pThread.join();
        cThread.join();
        pipedOutputStream.close();
        pipedInputStream.close();
    }

}
class Producer implements Runnable {
    private static final Logger log = LogManager.getLogger(Producer.class);

    private final PipedOutputStream pipedOutputStream;

    public Producer(PipedOutputStream pipedOutputStream) {
        this.pipedOutputStream = pipedOutputStream;
    }

    @Override
    public void run() {

        int index = 0;
        try {
            while (index <= 25) {
                log.info("Producer thread generating: " + index);
                pipedOutputStream.write(index);
                Thread.sleep(50);
                index++;
            }
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        } finally {
            try {
                pipedOutputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }
}

class Consumer implements Runnable {
    private static final Logger log = LogManager.getLogger(Consumer.class);

    private final PipedInputStream pipedInputStream;

    public Consumer(PipedInputStream pipedInputStream) {
        this.pipedInputStream = pipedInputStream;
    }

    @Override
    public void run() {
        try {
            while (true) {
                int value = pipedInputStream.read();
                log.info("Consumer thread consuming: " + value);
                Thread.sleep(50);
                if (value == 25)
                    break;
            }
        } catch (IOException | InterruptedException e) {
            log.error(e.getMessage());
        } finally {
            try {
                pipedInputStream.close();
            } catch (IOException e) {
                log.error(e.getMessage());
            }
        }
    }
}
