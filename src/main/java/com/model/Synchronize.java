package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Synchronize {
    private static final Logger LOGGER = LogManager.getLogger(Synchronize.class);
    static int number = 10;

    private static synchronized void add(){
        for (int i = 0; i < 10; i++) {
            number += i;
        }
        LOGGER.info("number = " + number);
    }

    private static synchronized void subtraction(){
        for (int i = 0; i < 10; i++) {
            number -= i;
        }
        LOGGER.info("number = " + number);

    }

    private static synchronized void multiplication(){
        number *= 2;
        LOGGER.info(number);
    }

    private static void showAllMethod(){
        Thread thread = new Thread(Synchronize::multiplication);
        thread.start();
        Thread thread1 = new Thread(Synchronize::add);
        thread1.start();
        Thread thread2 = new Thread(Synchronize::subtraction);
        thread2.start();

        try {
            thread.join();
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info(number);
    }


    public static void run(){
        ExecutorService executor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 3; i++) {
            executor.submit(new Thread(Synchronize::add, Thread.currentThread().getName()));
        }
        executor.shutdown();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info(number);
        LOGGER.info("Start all method");
        showAllMethod();
    }



}
