package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

public class PingPong {
    private static final Logger LOGGER = LogManager.getLogger(PingPong.class);
    private volatile  static int A = 0;
    private static Object sync = new Object();

    public void show(){
        LOGGER.info(LocalDateTime.now());
        ping().start();
        pong().start();

        try {
            ping().join();
            pong().join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Thread ping(){
        Thread ping = new Thread(() -> {
            synchronized (sync){
                while (true) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    A++;
                    sync.notify();
                    if(A > 20){
                        break;
                    }
                    LOGGER.info("ping " + A + " Thread = " + Thread.currentThread().getName());
                }
            }
        });
        return ping;
    }

    private Thread pong(){
        Thread pong = new Thread(() -> {
            synchronized (sync){
                while (true) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    A++;
                    if(A > 20){
                        break;
                    }
                    LOGGER.info("pong " + A + " Thread = " + Thread.currentThread().getName());
                }
            }
        });
        return pong;
    }
}
