package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class RandomAmount {
    private static final Logger LOGGER = LogManager.getLogger(Synchronize.class);
    public void run(){
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        for (int i = 0; i < 10; i++) {
            executorService.schedule(new Thread(() -> LOGGER.info(Thread.currentThread().getName())), new Random().nextInt(10), TimeUnit.SECONDS);
        }
        executorService.shutdown();
    }
}
