package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MyBlockingQueue {
    private static final Logger log = LogManager.getLogger(MyBlockingQueue.class);
    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(6);

    public void run(){
        Thread writer = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(200);
                    queue.put(i);
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
                log.info(Thread.currentThread().getName()+queue);
            }

        }, "writer");
        Thread remover = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(500);
                    queue.take();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
                log.info(Thread.currentThread().getName()+queue);
            }

        }, "remover");

        writer.start();
        remover.start();
    }
}
