package com.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchonizeWithLock {

    private static final Logger LOGGER = LogManager.getLogger(SynchonizeWithLock.class);
    private Lock lock = new ReentrantLock();
    static int number = 10;

    private void add(){
        try {
            lock.lock();
            for (int i = 0; i < 10; i++) {
                number += i;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            LOGGER.info("number = " + number);
            lock.unlock();
        }
    }

    private void subtraction(){
        try {
            lock.lock();
            for (int i = 0; i < 10; i++) {
                number -= i;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            LOGGER.info("number = " + number);
            lock.unlock();
        }

    }

    private void multiplication(){
        try {
            lock.lock();
            number *= 2;
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            LOGGER.info("number = " + number);
            lock.unlock();
        }
    }

    private void showAllMethod(){
        Thread thread = new Thread(() -> multiplication());
        thread.start();
        Thread thread1 = new Thread(() -> add());
        thread1.start();
        Thread thread2 = new Thread(() -> subtraction());
        thread2.start();

        try {
            thread.join();
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info(number);
    }


    public void run(){
        ExecutorService executor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 3; i++) {
            executor.submit(new Thread(() -> add(), Thread.currentThread().getName()));
        }
        executor.shutdown();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.info(number);
        LOGGER.info("Start all method");
        showAllMethod();
    }
}
