package com;

import com.view.MyView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger LOGGER = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        new MyView().run();

    }
}
